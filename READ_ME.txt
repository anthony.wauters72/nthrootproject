Conception :

Etablir la racine nieme d'un nombre réél positif supérieur à 1 revient à déterminer le seul zéro du polynome suivant :

P(x)=x^n - dividende.

Ce polynome est convexe et ne coupe l'axe des abcisses qu'une fois. (On peut uitliser le théorème des valeurs intermédiaires pour le prouver).

Par conséquent, la méthode de Newton (qui a pour but de déterminer les racines de fonctions) en venant de la droite est vouée à converger vers notre racine.

Optimisation lors de tests :

Au cours des tests l'écriture simple de la méthode de newton en calculant la valeur de la dérivée et de l'image du n-ième terme de la boucle occasionnait des infinis et demandait trop de calculs, d'où la simplification de la méthode pour minimiser l'utilisation de la fonction miseAPuissance.